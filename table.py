import enum
from typing import Dict
import json
import time

class TableStatus(enum.IntEnum):
    CLEAN = 0
    IN_USE = 1
    NEED_BUSSING = 2
    NEED_WIPING = 3

    def get_next(self):
        return type(self)((self + 1) % 4)


class TableEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Table):
            result = dict(obj.__dict__)
            result["status"] = [int(result["status"]), result["status"].name]
            return result
        elif isinstance(obj, TableType):
            return obj.name
        return json.JSONEncoder.default(self, obj)


class TableType(enum.Enum):
    BOOTH = enum.auto()
    TABLE = enum.auto()

class Table:

    def __init__(self, table_no:int, capacity:int, ttype: TableType):
        self.table_no = table_no
        self.capacity = capacity
        self.status = TableStatus.CLEAN
        self.ttype = ttype
        self.m_time = int(time.time())
        self.st_time = -1
    
    def next(self):
        self.status = self.status.get_next()
        if self.status == TableStatus.IN_USE:
            self.st_time = int(time.time())
    
    def touch(self):
        self.m_time = int(time.time())


    def __hash__(self):
        return self.table_no

    @classmethod
    def load(cls, file) -> Dict[int, "Table"]:
        import csv
        result = {}
        for line in csv.reader(file, delimiter=" "):
            table, capacity = int(line[0]), int(line[1])
            ttype = getattr(TableType, line[2])
            result[table] = cls(table, capacity, ttype)
        return result

from typing import List
import time
import json

class Group:

    def __init__(self, name, n_memb, wait_time, c_time=None):
        self.name = name
        self.n_memb = n_memb
        self.wait_time = wait_time
        self.c_time = int(time.time()) if c_time is None else c_time
        self.s_time = -1 # When this group is up next, this value gets set
    
    def set_time(self):
        self.s_time = int(time.time()) + self.wait_time


class GroupEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, Group):
            return obj.__dict__
        return obj


class WaitList(list):
    pass
"""
Usage: auto-host [options]

Options:
    --host=<HOST>   Bind address [default: localhost]
    --port=<PORT>   Bind port [default: 3000]
"""
import textwrap
import websockets
import flask
import threading
import docopt
import asyncio
import table
import logging
import json
from typing import *
import waitlist
from websockets import WebSocketClientProtocol

logger = logging.getLogger(__name__)
app = flask.Flask(__name__)

@app.route("/")
def index(*args):
    return textwrap.dedent(f"""
        <!DOCTYPE HTML>
        <html>
            <head>
            </head>
            <body>
                <div id="root"></div>
                <script>
                    window.addr = 'ws://{HOST}:{WS_PORT}'
                </script>
                <script src="webview.js"></script>
            <body>
        </html>
    """)


@app.route("/webview.js")
def webview(*args):
    return flask.send_from_directory("public", "webview.js")

@app.route("/touchicon")
def touchicon(*args):
    return flask.send_from_directory("public", "touchicon.png")


@app.after_request
def _(r: flask.Response):
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers["Cache-Control"] = "public, max-age=0"
    return r


class Encoder(json.JSONEncoder):

    def default(self, obj):
        # TODO combine all encoders
        return obj


class SocketServer:

    def __init__(self, host, port):
        self.clients = set()
        self.host = host
        self.port = port
        self.server = None
        self.loop = asyncio.new_event_loop()
        
        with open("table_data.txt", "r") as fp:
            self.tables = table.Table.load(fp)

        self.waitlist: List[waitlist.Group] = []

    def find_from_waitlist(self, group: waitlist.Group) -> Tuple[int, Optional[waitlist.Group]]:
        for i, grp in enumerate(self.waitlist):
            if grp.c_time == group.c_time:
                return i, grp
        return -1, None

    def name_in_waitlist(self, group: waitlist.Group) -> bool:
        for grp in self.waitlist:
            if grp.name == group.name:
                return True
        return False
    

    def __enter__(self):
        def thrd():
            asyncio.set_event_loop(self.loop)
            self.server = self.loop.run_until_complete(
                websockets.serve(self.request_handler, self.host, self.port)
            )
            self.loop.run_forever()
        self._thrd = threading.Thread(target=thrd, daemon=True)
        self._thrd.start()


    def __exit__(self, *args, **kwargs):
        if self.server is not None:
            self.server.close()
            task = asyncio.run_coroutine_threadsafe(
                self.server.wait_closed(), self.loop
            )
            @task.add_done_callback
            def _(*args):
                self.loop.stop()
        else:
            self.loop.stop()
    

    async def request_handler(self, ws:WebSocketClientProtocol, path:str):
        self.clients.add(ws)
        print(f"client joined (total: {len(self.clients)})")
        try:
            async for message in ws:
                message = json.loads(message)
                method:str = getattr(self, message["method"], None)

                if method is None:
                    logger.warning(f"Unknown method '{message['method']}'")
                    continue
                elif message["method"].startswith("init"):
                    print(f"Received {message['method']}()")
                    await method(ws, message["params"])
                else:
                    print(f"Received {message['method']}({message['params']})")
                    await method(message["params"])
        finally:
            self.clients.remove(ws)
            print(f"client left (total: {len(self.clients)})")


    async def init_table_state(self, ws: WebSocketClientProtocol, _):
        await ws.send(json.dumps({
                "method": "init_table_state",
                "params": self.tables,
            }, cls=table.TableEncoder))


    async def update_table_state(self, params):
        table_no = params["table_no"]
        self.tables[table_no].next()
        for client in self.clients:
            await client.send(json.dumps({
                "method": "update_table_state",
                "params": self.tables[table_no],
            }, cls=table.TableEncoder))


    async def touch_table(self, params):
        table_no = params["table_no"]
        self.tables[table_no].touch()
        for client in self.clients:
            await client.send(json.dumps({
                "method": "update_table_state",
                "params": self.tables[table_no],
            }, cls=table.TableEncoder))
    
    async def touch_all(self, params):
        for t in self.tables.values():
            if t.status == table.TableStatus.CLEAN:
                t.touch()

        message = json.dumps([
            {"method": "update_table_state", "params": t} for t in self.tables.values() if t.status == table.TableStatus.CLEAN
        ], cls=table.TableEncoder)

        for client in self.clients:
            await client.send(message)
    
    async def init_waitlist(self, ws, params=None):
        await ws.send(json.dumps({
                "method": "init_waitlist",
                "params": self.waitlist,
            }, 
            cls=waitlist.GroupEncoder,
        ))

    async def add_to_waitlist(self, params):
        group = waitlist.Group(
            params["name"],
            params["n_memb"],
            params["wait_time"],
        )

        group.set_time()

        if self.name_in_waitlist(group):
            print(f"ERROR: '{group.name}' in waitlist")
            return

        self.waitlist.append(group)
        for client in self.clients:
            await self.init_waitlist(client)
        


    async def move_from_waitlist(self, params):
        pass



    async def delete_from_waitlist(self, params):
        group = waitlist.Group(
            params["name"],
            params["n_memb"],
            params["wait_time"],
            params["c_time"],
        )

        ind, result = self.find_from_waitlist(group)
        if result is None:
            print(f"ERROR: Group '{group.name}' not in waitlist")
            return
        
        del self.waitlist[ind]
        for client in self.clients:
            await self.init_waitlist(client)


        


if __name__ == "__main__":
    asyncio.get_event_loop()
    args = docopt.docopt(__doc__)
    
    WS_PORT = int(args["--port"]) + 1
    HOST = args["--host"]

    with SocketServer(args["--host"], int(args["--port"]) + 1):
        app.run(args["--host"], args["--port"])


import * as ReactDOM from "react-dom"
import * as React from "react"

import App from "./app/app"

let handlers = {};
window.socket = new WebSocket(window.addr)

window.add_event_handler = (method, handler) =>
{
    if (! (method in handlers)) {
        handlers[method] = new Set()
    }
    handlers[method].add(handler)
}

window.remove_event_handler = (method, handler) =>
{   
    handlers[method].delete(handler)
}

function 
call_handlers(data)
{
    if (data.method in handlers) {
        console.debug(`received ${data.method}`)
        for (let handler of handlers[data.method]) {
            handler(data.params)
        }
    }
    else {
        console.warn(`No handlers registered for method '${data.method}'`)
    }
}

window.socket.onmessage = (event) =>
{
    let data = JSON.parse(event.data)
    if (Array.isArray(data)) {
        for (let x of data) {
            call_handlers(x);
        }
    }
    else {
        call_handlers(data);
    }
}


window.add_event_handler("init_table_state", (params) =>
{   
    console.log("Initiating app...")
    ReactDOM.render(
        <App initState={params}/>,
        document.getElementById("root")
    )
})

window.socket.onopen = () =>
{
    console.log("Socket opened")
    window.socket.send(JSON.stringify({
        method: "init_table_state",
        params: {},
    }))
}


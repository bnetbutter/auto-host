import React from "react";
import "./prompt.css"

export default 
class Prompt extends React.Component {

    constructor(props)
    {
        super(props)
        this.state = {
            status: this.props.status
        }
    }

    componentDidMount()
    {
        this.handler = (params) =>
        {
            this.setState({status: params.status[1]})
        }
        
        window.add_event_handler("update_table_state", this.handler)
    }

    componentWillUnmount()
    {
        window.remove_event_handler("update_table_state", this.handler)
    }

    render()
    {
        return <div id="Prompt" {...this.props}>
            <div className="PromptGrid">
                <div style={{
                            gridRow: "1"
                        }}>
                    {this.props.table_no}
                </div>
                <div style={{gridRow: "2"}}>
                    {this.state.status}
                </div>
                <div style={{
                            gridRow: "3"
                        }}>
                    <button className="PromptButton"
                        onClick={() => this.props.nextState(parseInt(this.props.table_no))}
                    >
                        Next State
                    </button>
                </div>
                <div style={{
                            gridRow: "4"
                        }}>
                    <button className="PromptButton"
                        onClick={this.props.hidePrompt}
                    >
                        Done
                    </button>
                </div>
            </div>
        </div>
    }
}

import * as React from "react"
import Blink from "./blinker"
import "./floorplan.css"
import "regenerator-runtime"

// keys map to enum values defined in table.py
let CODES = {
    0: "green",
    1: "yellow",
    2: "red",
    3: "cyan",
}

// keys are arbitrary
let BLINK_STAGE = {
    0: 0, // No blink
    1: 1.0, // Once per sec
    2: 0.5, // Twice
    3: 0.25 // etc...
};

let normal_unit = (s) => s * 60 // Change to 60 for minutes

// The faster the blink, the less confident the user should be about the 
// accuracy of the information displayed on screen. A fast blink tells the user
// "Go check this table and make sure it's actually clean, or actually in use"
// Or it can serve as a reminder to go check up on a customer or clean this table
// because it hasn't been cleaned for a long time
let DECAY_DISCRETIZATION = {
    0: { // CLEAN
        20: BLINK_STAGE[0],
        30: BLINK_STAGE[1],
        35: BLINK_STAGE[2],
        40: BLINK_STAGE[3],
    },
    1: { // IN_USE
        20: BLINK_STAGE[0],
        40: BLINK_STAGE[1],
        50: BLINK_STAGE[2],
        55: BLINK_STAGE[3],
    },
    2: { // NEED_BUSSING
        10: BLINK_STAGE[0],
        20: BLINK_STAGE[1],
        25: BLINK_STAGE[2],
        30: BLINK_STAGE[3],
    },
    3: { // NEED_WIPING
        5: BLINK_STAGE[0],
        8: BLINK_STAGE[1],
        10: BLINK_STAGE[2],
        Infinity: BLINK_STAGE[3],
    },
}



export default
class Table extends React.Component {
    
    constructor(props)
    {
        super(props)
        this.state = {
            blinkrate: 2
        }

        this.onClick = this.onClick.bind(this)
        new Promise(async () =>
        {
            while (1) {
                await new Promise(r => setTimeout(r, 1000))
                this.setState({...this.state, blinkrate: this._get_blinkrate()})
            }
        })
    }

    _get_blinkrate()
    {
        let elapsed = this._seconds_elapsed()
        let blink_table = DECAY_DISCRETIZATION[this.props.table.status[0]]
        let key;
        for (key of Object.keys(blink_table)) {
            if (elapsed <= normal_unit(key)) {
                return blink_table[key] 
            }
        }
        return blink_table[key]
    }

    _seconds_elapsed()
    {
        let m_time = this.props.table.m_time
        return Math.floor(Date.now() / 1000) - m_time;
    }

    componentDidMount()
    {
        console.debug(`Table ${this.props.children} mounted`)
        this.update_handler = (params) =>
        {
            if (params.table_no === this.props.children) {
                this.setState({
                    ...this.state,
                    color_code: CODES[params.status[0]]
                })
            }
        }

        window.add_event_handler("update_table_state", this.update_handler)
    }

    componentWillUnmount()
    {
        console.debug(`Table ${this.props.children} unmounting`)
        window.remove_event_handler("update_table_state", this.update_handler)
    }

    onClick()
    {
        // Remove current prompt window from DOM
        window.socket.send(JSON.stringify({
            method: "update_table_state",
            params: {
                table_no: this.props.table.table_no
            }
        }))
    }

    render()
    {
        let backgroundColor = CODES[this.props.table.status[0]]
        let color = this.props.last_seated === this.props.table.table_no ? "blue" : ""
        let borderWidth = this.props.last_seated === this.props.table.table_no ? "5px" : ""

        return <button className="Table"
                style={{
                    ...this.props.style,
                    backgroundColor,
                    display: this.state.display,
                    borderColor: color,
                    color,
                    borderWidth,
                }}
                onClick={this.onClick}
        >
            <Blink blinkrate={this.state.blinkrate}>{this.props.children}</Blink>
        </button>
    }
}

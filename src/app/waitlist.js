import * as React from "react"
import { render } from "react-dom";
import "regenerator-runtime"
import "./waitlist.css"

function
MenuButton(props)
{

    let onClick = () => 
    {
        console.debug("Clicked menu button");
        props.onClick()
    }

    return <button id="MenuButton" {...props} onClick={onClick}>
        <div className="menudiv"/>
        <div className="menudiv"/>
        <div className="menudiv"/>
    </button>
}

function
ElapsedTime(props)
{
    let elapsed = parseInt(props.children)
    let min = `${Math.floor(elapsed/60)}`.padStart(2, " ")
    let sec = `${elapsed % 60}`.padStart(2, "0");
    return <span>{min}:{sec}</span>
}

class CountDown extends React.Component {
    constructor(props)
    {
        super(props)
        this.state = {
            elapsed: 0
        }

        new Promise(async (resolve) => {
            this.resolve = resolve;
            let current_time = Math.floor(Date.now()/1000)
            while (true) {
                await new Promise((r) => setTimeout(r, 1000));
                let difftime = this.props.time - current_time
                if (difftime < 0) {
                    if (this.props.onTimeout) {
                        console.debug("Timeout");
                        this.props.onTimeout()
                    }
                    return resolve()
                }
                else {
                    current_time += 1
                    this.setState({elapsed: difftime})
                }
            }
        })
    }

    componentWillUnmount()
    {
        try {
            this.resolve()
        }
        catch {

        }
    }

    render()
    {
        return <ElapsedTime>{this.state.elapsed}</ElapsedTime>
    }

}

function
TouchButton(props)
{
    return <button id="TouchButton" {...props}>
        <img id="touchicon" src="./touchicon" />
    </button>
}


class GroupSummary extends React.Component {

    constructor(props)
    {
        super(props)
        this.state = {
            backgroundColor: ""
        };

        this.on_timeout = () =>
        {
            this.setState({backgroundColor: "red"})
        }
    }

    render()
    {
        return <div className="group-summary" {...this.props} style={{display: "grid", backgroundColor: this.state.backgroundColor}}>
            <span className="GroupN">
                <div style={{fontSize: "large"}}>
                    Top
                </div>
                <div>
                    {this.props.group.n_memb}
                </div>
            </span>
            <span className="GroupT">
                <div style={{fontSize: "large"}}>
                    Time
                </div>
                <div>
                    <CountDown onTimeout={this.on_timeout} time={this.props.group.s_time} />
                </div>
            </span>
        </div>
    }
}

class GroupVerbose extends GroupSummary {

    on_delete()
    {
        window.socket.send(JSON.stringify({
            method:"delete_from_waitlist",
            params: this.props.group
        }))
    }

    render()
    {
        return <div className="group-verbose" {...this.props} style={{display: "grid", backgroundColor: this.state.backgroundColor}}>
            
            <button className="GroupBt" onClick={() => this.on_delete()}>
                Del
            </button>
        
            <span className="GroupName">
                <div style={{fontSize: "large"}}>
                    Name
                </div>
                <div>
                    {this.props.group.name}
                </div>
            </span>
            <span className="GroupN">
                <div style={{fontSize: "large"}}>
                    Top
                </div>
                <div>
                    {this.props.group.n_memb}
                </div>
            </span>
            <span className="GroupT">
                <div style={{fontSize: "large"}}>
                    Time
                </div>
                <div>
                    <CountDown onTimeout={this.on_timeout} time={this.props.group.s_time} />
                </div>
            </span>
        </div>
    }
}



function
GroupForm(props)
{
    let onClick = () =>
    {
        console.debug("Group form Add")
        let name_doc = document.getElementById("group-name")
        let nmemb_doc = document.getElementById("group-nmemb")
        let wait_doc = document.getElementById("group-wait-time")
        
        let name = name_doc.value
        let nmemb = parseInt(nmemb_doc.value)
        let wait = parseInt(wait_doc.value) * 60

        if ((! name) || isNaN(nmemb) || isNaN(wait)) {
            console.log(`${name} ${nmemb === NaN} ${wait === NaN}`)
            console.warn("Invalid input")
            return false
        }
        else {
            window.socket.send(JSON.stringify({
                method:"add_to_waitlist",
                params: {
                    name,
                    n_memb: nmemb,
                    wait_time: wait,       
                }
            }))
        }
    }

    return <div className="group-verbose" {...props} style={{display: "grid", width:"100%"}}>
            
        <button className="GroupBt" onClick={() => onClick()}>
            Add
        </button>
        <span className="GroupName">    
            <div style={{fontSize: "large"}}>
                Name
            </div>
            <input type="text" id="group-name" name="group-name" size="10"/>
        </span>
        <span className="GroupN">
            <div style={{fontSize: "large"}}>
                Top
            </div>
            <input type="text" id="group-nmemb" name="group-nmemb" maxLength="2" size="2"/>
        </span>
        <span className="GroupT">
            <div style={{fontSize: "large"}}>
                Time
            </div>
            <input type="text" id="group-wait-time" name="group-wait-time" maxLength="2" size="2"/>
        </span>
    </div>
}


class WaitlistSummary extends React.Component {
    constructor(props)
    {
        super(props)
        this.state = {
            groups:[],
        }
    }

    componentDidMount()
    {
        this.handler = (params) => this.setState({...this.state, groups: params})
        window.add_event_handler("init_waitlist", this.handler)
        window.socket.send(JSON.stringify({
            method: "init_waitlist",
            params: {},
        }))
    }

    componentWillUnmount()
    {
        window.remove_event_handler("init_waitlist", this.handler)
    }

    render()
    {
        return <div id="WaitList-Summary">
            <div style={{height:"100%", overflowY: "scroll"}}>
            {
                this.state.groups.map((elt, ind) =>
                {
                    return <GroupSummary key={ind} group={elt} ind={ind}/>
                })
            }
            </div>
        </div>
    }
}

class WaitListVerbose extends WaitlistSummary {
    
    render()
    {
        return <div id="WaitList-Verbose">
            <div style={{height:"100%", overflowY: "scroll", backgroundColor: "white"}}>
            {
                this.state.groups.map((elt, ind) =>
                {
                    return <GroupVerbose key={ind} group={elt} ind={ind} />
                })
            }
            </div>
            <GroupForm />
        </div>
    }
}

export default
class Waitlist extends React.Component {
    
    constructor(props)
    {
        super(props)
        this.state = {
            fullview: false,
        } 
        this.toggle_view = () =>
        {
            this.setState({...this.state, fullview: this.state.fullview ? false : true})
        }
    }
    
    render()
    {
        return <div id="WaitList" >
            <div style={{display: "grid", gridTemplateColumns:"1fr 1fr"}}>
                <TouchButton onClick={
                    () => window.socket.send(JSON.stringify({
                            method: "touch_all",
                            params: {},
                        }))
                    }/>
                <MenuButton onClick={this.toggle_view}/>
            </div>
            {
                this.state.fullview ? <WaitListVerbose /> : <WaitlistSummary />
            }
           
        </div>
    }
}
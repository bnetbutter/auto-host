import * as React from "react"
import Prompt from "./prompt.js"
import Table from "./floorplan.js"
import WaitList from "./waitlist"
import "./app.css"
import "./floorplan.css"

export default
class App extends React.Component {

    constructor(props)
    {
        super(props)
        this.state = {
            show_window: false,
            prompt_table: -1,
            prompt_state: "", // Shows the state of selected table
            table_states: {...this.props.initState} // State of all tables
        }
        window.add_event_handler("update_table_state", (params) =>
        {
            let states = this.state.table_states
            states[params.table_no] = {...states[params.table_no], ...params}
            this.setState({...this.state})
        })

        this.show_prompt_window = (table_no) =>
        {
            console.debug("clicked " + table_no)
            if (this.state.prompt_table < 0) {
                this.setState({
                    ...this.state,
                    prompt_table: table_no,
                    prompt_state: this.state.table_states[table_no].status[1]
                })
            }
        }

        this.hide_prompt_window = () =>
        {
            console.debug("hide_prompt_window")
            this.setState({
                ...this.state,
                prompt_table: -1,
            })
        }
        // Increment the table status
        this.next_state = (table_no, n_user) =>
        {
            window.socket.send(JSON.stringify({
                method: "update_table_state",
                params: {
                    table_no,
                    n_user,
                }
            }))

        }

        this.touch_table = (table_no) =>
        {   
            
            this.hide_prompt_window()
            window.socket.send(JSON.stringify({
                method: "touch_table",
                params: {
                    table_no,
                }
            }))

        }

        this.notify_table_state = (table_no) =>
        {

        }

    }



    render()
    {  

        let last_seated = () =>
        {

            let tables = Object.values(this.state.table_states)
            let last_table = tables[0]
            
            for (let table of tables) {
                if (table.st_time > last_table.st_time) {
                    last_table = table
                }
            }
            return last_table.table_no
        }


        let props = {
            showPrompt: this.show_prompt_window,
            hidePrompt: this.hide_prompt_window,
            touch: this.touch_table,
            last_seated: last_seated()
        }

        return <div className="App">
            <span style={{display: "inline-block"}}>
            {
                (() =>
                {
                    return this.state.prompt_table > 0? <Prompt
                        style={{
                                display: "block",
                                position: "fixed",
                            }}
                        table_no={this.state.prompt_table}
                        hidePrompt={this.hide_prompt_window}
                        nextState={this.next_state}
                        touch={this.touch_table}
                        status={this.state.table_states[this.state.prompt_table].status[1]}
                    /> : ""
                })()

            }
            <div id="FloorPlan" style={{display: "grid"}}>
                <Table {...{...props, table: this.state.table_states[21]}} style={{gridColumn: "1 / 3", gridRow: "1/4"}} >21</Table>
                <Table {...{...props, table: this.state.table_states[20]}} style={{gridColumn: "3", gridRow: "1/3"}}>20</Table>
                <Table {...{...props, table: this.state.table_states[19]}} style={{gridColumn: "5/7", gridRow: "1"}}>19</Table>
                <Table {...{...props, table: this.state.table_states[18]}} style={{gridColumn: "7/9", gridRow: "1"}}>18</Table>
                <Table {...{...props, table: this.state.table_states[17]}} style={{gridColumn: "10", gridRow: "1/4"}}>17</Table>
                <Table {...{...props, table: this.state.table_states[15]}} style={{gridColumn: "11", gridRow: "1/4"}}>15</Table>
                <Table {...{...props, table: this.state.table_states[14]}} style={{gridColumn: "12 / 14", gridRow: "1/1"}}>14</Table>
                <Table {...{...props, table: this.state.table_states[12]}} style={{gridColumn: "14 / 16", gridRow: "1/1"}}>12</Table>
                <Table {...{...props, table: this.state.table_states[11]}} style={{gridColumn: "16 / 18", gridRow: "1/1"}}>11</Table>

                <Table {...{...props, table: this.state.table_states[39]}} style={{gridColumn: "1/3", gridRow: "5"}}>39</Table>
                <Table {...{...props, table: this.state.table_states[38]}} style={{gridColumn: "4/6", gridRow: "5"}}>38</Table>
                <Table {...{...props, table: this.state.table_states[37]}} style={{gridColumn: "6", gridRow: "5"}}>37</Table>
                <Table {...{...props, table: this.state.table_states[36]}} style={{gridColumn: "8", gridRow: "5"}}>36</Table>
                <Table {...{...props, table: this.state.table_states[35]}} style={{gridColumn: "10", gridRow: "5/7"}}>35</Table>
                <Table {...{...props, table: this.state.table_states[34]}} style={{gridColumn: "11", gridRow: "5/7"}}>34</Table>
                <Table {...{...props, table: this.state.table_states[33]}} style={{gridColumn: "13", gridRow: "5/7"}}>33</Table>
                <Table {...{...props, table: this.state.table_states[32]}} style={{gridColumn: "14", gridRow: "5/7"}}>32</Table>
                <Table {...{...props, table: this.state.table_states[31]}} style={{gridColumn: "16/18", gridRow: "5"}}>31</Table>

                <Table {...{...props, table: this.state.table_states[48]}} style={{gridColumn: "1/3", gridRow: "6"}}>48</Table>
                <Table {...{...props, table: this.state.table_states[47]}} style={{gridColumn: "4/7", gridRow: "6"}}>47</Table>
                <Table {...{...props, table: this.state.table_states[46]}} style={{gridColumn: "8", gridRow: "6"}}>46</Table>
                <Table {...{...props, table: this.state.table_states[45]}} style={{gridColumn: "10", gridRow: "7/9"}}>45</Table>
                <Table {...{...props, table: this.state.table_states[44]}} style={{gridColumn: "11", gridRow: "7/9"}}>44</Table>
                <Table {...{...props, table: this.state.table_states[43]}} style={{gridColumn: "13", gridRow: "7/9"}}>43</Table>
                <Table {...{...props, table: this.state.table_states[42]}} style={{gridColumn: "14", gridRow: "7/9"}}>42</Table>
                <Table {...{...props, table: this.state.table_states[41]}} style={{gridColumn: "16/18", gridRow: "7"}}>41</Table>

                <Table {...{...props, table: this.state.table_states[54]}} style={{gridColumn: "1/3", gridRow: "7"}}>54</Table>
                <Table {...{...props, table: this.state.table_states[53]}} style={{gridColumn: "4", gridRow: "7"}}>53</Table>
                <Table {...{...props, table: this.state.table_states[52]}} style={{gridColumn: "5/7", gridRow: "7"}}>52</Table>
                <Table {...{...props, table: this.state.table_states[51]}} style={{gridColumn: "8", gridRow: "7"}}>51</Table>
            </div>
            </span>
            <WaitList />
        </div>
    }
} 
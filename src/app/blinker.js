import * as React from "react"

export default
class Blink extends React.Component {

    constructor(props)
    {
        super(props)
        this.state = {
            visibility: "visible"
        }
    }

    render()
    {
        return <div style={{
            ... (this.props.style ? this.props.style : {}),
            visibility: this.state.visibility
        }}>{this.props.children}</div>
    }
}

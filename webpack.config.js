var path = require("path");

module.exports = {
  entry: {
    webview: "./build/index.js"
  },
  output: {
    path: path.join(__dirname, "public"),
    filename: "[name].js"
  },
  devtool: "eval-source-map",
  resolve: {
    extensions: [".js"]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          }
        ]
      },
    ]
  },
  performance: {
    hints: false
  }
};